<?php

class Comment
{
	private $id;

	private $content;

	private $user_id;

	public function __construct($id, $content, $user_id)
	{
		$this->id = $id;
		$this->content = $content;
		$this->user_id = $user_id;
	}
}