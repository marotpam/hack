<?php

class Post
{

	private $likes_count;

	private array $comments;

	public function __construct($id, $title, $content)
	{
		$this->id = $id;
		$this->title = $title;
		$this->content = $content;
		$this->likes_count = 0;
		$this->comments = array();
	}

	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($new_title)
	{
		$this->title = $new_title;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setContent($new_content)
	{
		$this->content = $new_content;
	}

	public function getComments()
	{
		return $this->comments;
	}

	public function addComment(&$new_comment)
	{
		$this->comments[] = $new_comment;
	}

	public function getLikesCount()
	{
		return $this->likes_count;
	}

	public function addLike()
	{
		$this->likes_count++;
	}
}
