<?php

require 'Pager.php';
require 'Post.php';
require 'PostRepository.php';
require 'Comment.php';

function main()
{
	$posts_repository = new PostRepository();
	$posts = $posts_repository->getAllPosts();

	printPosts($posts);
}

function printPosts($posts)
{
	if (empty($posts))
	{
		echo "No posts to display";
	}
	else
	{
		foreach( $posts as $post )
		{
			echo "<b>" . $post->getId() . ": " . $post->getTitle() . "</b><br>";
			echo $post->getContent() . "<br>";
			echo "<em>Likes count: " . $post->getLikesCount() . "</em><br>";
			echo "<br><hr>";
		}
	}
}

main();
